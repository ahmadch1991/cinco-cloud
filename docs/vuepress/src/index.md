---
home: true
heroImage: /assets/cinco-logo.png
tagline: Documentation for developing, operating and using CincoCloud.
actionText: Quick Start →
actionLink: /content/introduction/
footer: Made by LS5 TU Dortmund
---
