package info.scce.cincocloud.db;

public enum ProjectType {
  LANGUAGE_EDITOR,
  MODEL_EDITOR
}
