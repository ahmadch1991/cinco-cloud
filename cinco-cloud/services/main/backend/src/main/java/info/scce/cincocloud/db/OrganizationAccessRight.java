package info.scce.cincocloud.db;

public enum OrganizationAccessRight {
  CREATE_PROJECTS,
  EDIT_PROJECTS,
  DELETE_PROJECTS
}
