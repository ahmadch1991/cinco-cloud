package info.scce.cincocloud.core.rest.tos;

import com.fasterxml.jackson.annotation.JsonProperty;
import info.scce.cincocloud.db.ProjectDB;
import info.scce.cincocloud.db.UserDB;
import info.scce.cincocloud.db.UserSystemRole;
import info.scce.cincocloud.rest.ObjectCache;
import info.scce.cincocloud.rest.RESTBaseImpl;
import java.util.List;

public class UserTO extends RESTBaseImpl {

  private List<ProjectTO> personalProjects = new java.util.LinkedList<>();
  private List<UserSystemRole> systemRoles = new java.util.LinkedList<>();
  private String name;
  private String username;
  private String email;
  private FileReferenceTO profilePicture;
  private boolean isActivated;
  private boolean isDeactivatedByAdmin;

  public static UserTO fromEntity(final UserDB entity, final ObjectCache objectCache) {
    if (objectCache.containsRestTo(entity)) {
      return objectCache.getRestTo(entity);
    }

    final var result = new UserTO();
    result.setId(entity.id);
    result.setemail(entity.email);
    result.setname(entity.name);
    result.setusername(entity.username);
    result.setactivated(entity.isActivated);
    result.setdeactivatedByAdmin(entity.isDeactivatedByAdmin);

    if (entity.profilePicture != null) {
      result.setprofilePicture(new FileReferenceTO(entity.profilePicture));
    }

    objectCache.putRestTo(entity, result);

    for (ProjectDB p : entity.personalProjects) {
      result.getPersonalProjects().add(ProjectTO.fromEntity(p, objectCache));
    }

    for (UserSystemRole p : entity.systemRoles) {
      result.getsystemRoles().add(p);
    }

    return result;
  }

  @JsonProperty("personalProjects")
  public List<ProjectTO> getPersonalProjects() {
    return this.personalProjects;
  }

  @JsonProperty("personalProjects")
  public void setPersonalProjects(final List<ProjectTO> personalProjects) {
    this.personalProjects = personalProjects;
  }

  @JsonProperty("systemRoles")
  public List<UserSystemRole> getsystemRoles() {
    return this.systemRoles;
  }

  @JsonProperty("systemRoles")
  public void setsystemRoles(final List<UserSystemRole> systemRoles) {
    this.systemRoles = systemRoles;
  }

  @JsonProperty("name")
  public String getname() {
    return name;
  }

  @JsonProperty("name")
  public void setname(String name) {
    this.name = name;
  }

  @JsonProperty("username")
  public String getusername() {
    return this.username;
  }

  @JsonProperty("username")
  public void setusername(final String username) {
    this.username = username;
  }

  @JsonProperty("email")
  public String getemail() {
    return this.email;
  }

  @JsonProperty("email")
  public void setemail(final String email) {
    this.email = email;
  }

  @JsonProperty("profilePicture")
  public FileReferenceTO getprofilePicture() {
    return this.profilePicture;
  }

  @JsonProperty("profilePicture")
  public void setprofilePicture(final FileReferenceTO profilePicture) {
    this.profilePicture = profilePicture;
  }

  @JsonProperty("activated")
  public boolean getactivated() {
    return this.isActivated;
  }

  @JsonProperty("activated")
  public void setactivated(boolean activated) {
    this.isActivated = activated;
  }

  @JsonProperty("deactivatedByAdmin")
  public boolean getdeactivatedByAdmin() {
    return this.isDeactivatedByAdmin;
  }

  @JsonProperty("deactivatedByAdmin")
  public void setdeactivatedByAdmin(boolean deactivatedByAdmin) {
    this.isDeactivatedByAdmin = deactivatedByAdmin;
  }

}
