export enum BuildJobLogStatus {
  COMPLETE = 'COMPLETE',
  PARTIAL = 'PARTIAL',
  MISSING = 'MISSING'
}
