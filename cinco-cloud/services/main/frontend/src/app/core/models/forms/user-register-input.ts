export class UserRegisterInput {
  username: string;
  email: string;
  name: string;
  password: string;
  passwordConfirm: string;
}
