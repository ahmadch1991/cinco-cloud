export class WorkspaceImageBuildResult {
  projectId: number;
  success: boolean;
  message: string;
  image: string;
}
