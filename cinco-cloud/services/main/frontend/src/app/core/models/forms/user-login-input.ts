export class UserLoginInput {
  emailOrUsername: string;
  password: string;
}
