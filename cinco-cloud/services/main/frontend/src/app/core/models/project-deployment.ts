export interface ProjectDeployment {
  url: string;
  status: string;
}
