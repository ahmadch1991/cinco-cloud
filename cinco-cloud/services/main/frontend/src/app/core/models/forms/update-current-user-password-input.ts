export class UpdateCurrentUserPasswordInput {
  oldPassword: string;
  newPassword: string;
}
