apiVersion: v1
kind: ConfigMap
metadata:
  name: main-config
data:
  DATABASE_URL: "postgres-service/cc"
  AMPQ_HOST: "ampq-service"
  AMPQ_PORT: "5672"
  MINIO_HOST: "minio-service"
  MINIO_PORT: "9000"
  KUBERNETES_NAMESPACE: {{ .Release.Namespace }}
  CINCO_CLOUD_HOST: {{ .Values.ingress.host }}
  CINCO_CLOUD_SSL: {{ .Values.ssl | quote }}
  CINCO_CLOUD_MAILER_HOST: {{ .Values.main.mailer.host }}
  CINCO_CLOUD_MAILER_PORT: {{ .Values.main.mailer.port | quote }}
  CINCO_CLOUD_MAILER_SSL: {{ .Values.main.mailer.ssl | quote }}
  CINCO_CLOUD_MAILER_FROM: {{ .Values.main.mailer.from }}
  {{ if eq .Values.serverTier "local" }}
  ARCHETYPE_IMAGE: registry.gitlab.com/scce/cinco-cloud/archetype:local
  {{- else -}}
  ARCHETYPE_IMAGE: {{ .Values.archetype.image }}
  {{- end }}
  ARCHETYPE_STORAGE_CLASS_NAME: {{ .Values.storageClassName }}
  ARCHETYPE_STORAGE: {{ .Values.archetype.storage }}
  ARCHETYPE_HOST_PATH: {{ .Values.archetype.hostPath }}
  ARCHETYPE_CREATE_PERSISTENT_VOLUMES: {{ .Values.createPersistentVolumes | quote }}
  ENVIRONMENT: {{ .Values.serverTier }}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: main-statefulset
  labels:
    app: main
spec:
  serviceName: main
  replicas: 1
  selector:
    matchLabels:
      app: main
  template:
    metadata:
      labels:
        app: main
    spec:
      serviceAccountName: main-sa
      containers:
        - name: main
          image: {{ .Values.mainImage }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          ports:
            - containerPort: 8000
            - containerPort: 9000
            {{ if eq .Values.serverTier "local" }}
            - containerPort: 4200
            {{- end }}
          envFrom:
            - configMapRef:
                name: main-config
          env:
            - name: CINCO_CLOUD_PASSWORD_SECRET
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: passwordSecret
            - name: DATABASE_USER
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: databaseUser
            - name: DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: databasePassword
            - name: AMPQ_USERNAME
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: artemisUser
            - name: AMPQ_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: artemisPassword
            - name: MINIO_ACCESS_KEY
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: minioAccessKey
            - name: MINIO_SECRET_KEY
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: minioSecretKey
            - name: AUTH_PUBLIC_KEY
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: authPublicKey
            - name: AUTH_PRIVATE_KEY
              valueFrom:
                secretKeyRef:
                  name: cinco-cloud-main-secrets
                  key: authPrivateKey
          securityContext:
            privileged: true
          volumeMounts:
            - name: pv-data
              mountPath: /app/data
      volumes:
        - name: pv-data
          persistentVolumeClaim:
            claimName: main-pv-claim
---
apiVersion: v1
kind: Service
metadata:
  name: main-service
  labels:
    app: main
spec:
  ports:
    - port: 8000
      name: main
      protocol: TCP
    - port: 9000
      name: main-grpc
      protocol: TCP
    {{ if eq .Values.serverTier "local" }}
    - port: 4200
      name: main-frontend
      protocol: TCP
    {{- end }}
  {{ if eq .Values.serverTier "local" }}
  type: NodePort
  {{- else -}}
  type: ClusterIP
  {{- end }}
  selector:
    app: main
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: main-role-{{ .Release.Namespace }}
rules:
  - apiGroups: [""]
    resources: ["pods", "endpoints", "ingresses", "ingress", "deployments", "services", "configmaps", "statefulsets", "persistentvolumes", "persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
  - apiGroups: ["apps"]
    resources: ["deployments", "statefulsets"]
    verbs: [ "get", "list", "watch", "create", "update", "patch", "delete" ]
  - apiGroups: [ "extensions" ]
    resources: [ "deployments" ]
    verbs: [ "get", "list", "watch", "create", "update", "patch", "delete" ]
  - apiGroups: ["networking.k8s.io"]
    resources: ["ingresses", "ingress"]
    verbs: [ "get", "list", "watch", "create", "update", "patch", "delete" ]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: main-{{ .Release.Namespace }}
subjects:
  - kind: ServiceAccount
    namespace: {{ .Release.Namespace }}
    name: main-sa
roleRef:
  kind: ClusterRole
  name: main-role-{{ .Release.Namespace }}
  apiGroup: "rbac.authorization.k8s.io"
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: main-sa
---
{{ if eq .Values.createPersistentVolumes true }}
apiVersion: v1
kind: PersistentVolume
metadata:
  name: main-pv-volume
  labels:
    type: local
    app: main
spec:
  storageClassName: {{ .Values.storageClassName }}
  capacity:
    storage: {{ .Values.main.storage | default "10Gi" | quote }}
  claimRef:
    namespace: {{ .Release.Namespace }}
    name: main-pv-claim
  accessModes:
    - ReadWriteMany
  hostPath:
    path: {{ .Values.main.hostPath }}
{{ end }}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: main-pv-claim
  labels:
    app: main
spec:
  storageClassName: {{ .Values.storageClassName }}
  {{ if eq .Values.createPersistentVolumes true }}
  volumeName: main-pv-volume
  {{ end }}
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: {{ .Values.main.storage | default "10Gi" | quote }}
