# The name of the environment. Valid values are 'production' and 'local'.
serverTier: production

# If CincoCloud is started in debug mode. Should be 'false' if serverTier is 'production'.
debug: false

# Indicates that CincoCloud runs behind a ssl terminating reverse proxy, i.e., is reached by using https.
ssl: true

# If true, a mailcatcher gets started as part of the deployment stack.
mailcatcher: true

# The storage class name to use for persistent volumes.
storageClassName: standard

# The policy with which images are pulled. Should be 'Never' in local development mode and 'IfNotPresent' in production.
imagePullPolicy: IfNotPresent

# If persistent volumes are created by hand. 'false' if volumes are created automatically by claims. 
createPersistentVolumes: false

# The image of the main service.
mainImage: registry.gitlab.com/scce/cinco-cloud/main:latest

# The image of the ampq service.
ampqImage: registry.gitlab.com/scce/docker-images/activemq-artemis:2.17.0

# The image of the workspace builder service.
workspaceBuilderImage: registry.gitlab.com/scce/cinco-cloud/workspace-builder:latest

# Configuration for the ingress controller.
ingress:
  # The host where CincoCloud is deployed.
  host: app.example.com
  # The class for nginx.
  class: public

# Configuration for the cert manager.
certManager:
  # The email the certificates are issued to.
  email: demo@example.com

# Configuration for the main service.
main:
  mailer:
    host: mailcatcher-service
    port: 1025
    ssl: false
    from: cincocloud@example.com
  # The amount of available storage.
  storage: 10Gi
  # The path on the host machine where files are stored
  hostPath: /mnt/data/cincocloud/main

# Configuration for the postgres database.
postgres:
  # The amount of available storage.
  storage: 10Gi
  # The path on the host machine where files are stored
  hostPath: /mnt/data/cincocloud/postgres

# Configuration for the workspace builder service.
workspaceBuilder:
  # The amount of replicas available.
  replicas: 1

# Configuration for the message queue.
artemis:
  # The amount of available storage.
  storage: 2Gi
  # The path on the host machine where files are stored
  hostPath: /mnt/data/cincocloud/artemis

# Configuration for the minio object storage.
minio:
  # The amount of available storage.
  storage: 20Gi
  # The path on the host machine where files are stored
  hostPath: /mnt/data/cincocloud/minio

# Configuration for the archetype image and Theia editors.
archetype:
  # The image that is used for editors.
  image: registry.gitlab.com/scce/cinco-cloud/archetype:main-3f8d48f448d7a8b7d1db383c79e171656822819f
  # The amount of available storage.
  storage: 2Gi
  # The path on the host machine where files are stored
  hostPath: /mnt/data/cincocloud/archetype
  
