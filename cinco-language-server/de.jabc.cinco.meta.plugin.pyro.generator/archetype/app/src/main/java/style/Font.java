package style;


public interface Font {

    String getFontName();

    void setFontName(String value);

    int getSize();

    void setSize(int value);

    boolean isIsBold();

    void setIsBold(boolean value);

    boolean isIsItalic();

    void setIsItalic(boolean value);

}
