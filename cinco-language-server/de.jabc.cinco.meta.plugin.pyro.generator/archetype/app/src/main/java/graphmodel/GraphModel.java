package graphmodel;

public interface GraphModel extends ModelElementContainer {
	public void deleteModelElement(ModelElement cme);
}
