package graphmodel;

public interface BendingPoint {
    public long getX();
    public long getY();
}