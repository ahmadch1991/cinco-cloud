package info.scce.pyro.core.rest.types;

public class FileReference {

	private long id;
	private String fileName;
	private String contentType;
	private String filePath;

	public FileReference() {}

	public FileReference(final entity.core.BaseFileDB delegate) {
		this.setId(delegate.id);
		if(delegate.fileExtension != null)
			this.setFileName(delegate.filename + "." + delegate.fileExtension);
		else
			this.setFileName(delegate.filename);
		this.setContentType(delegate.contentType);
		this.setFilePath(delegate.path);
	}

	@com.fasterxml.jackson.annotation.JsonProperty("id")
	public long getId() {
		return id;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("id")
	public void setId(long id) {
		this.id = id;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("fileName")
	public String getFileName() {
		return fileName;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("fileName")
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("filePath")
	public String getFilePath() {
		return filePath;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("filePath")
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("contentType")
	public String getContentType() {
		return contentType;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("contentType")
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@java.lang.Override
	public boolean equals(final java.lang.Object obj) {
		if (this == obj) {
			return true;
		} else if (!(obj instanceof FileReference)) {
			return false;
		}
		final FileReference that = (FileReference) obj;
		if (this.getId() == -1 && that.getId() == -1) {
			return this == that;
		}
		return this.getId() == that.getId();
	}
}