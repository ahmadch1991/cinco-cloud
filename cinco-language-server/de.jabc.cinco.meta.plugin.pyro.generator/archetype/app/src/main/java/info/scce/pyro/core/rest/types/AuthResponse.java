package info.scce.pyro.core.rest.types;

public class AuthResponse {
	
	public String token;
	
	public AuthResponse(String t) {
		token = t;
	}
}