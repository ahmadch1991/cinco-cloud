# Pyro-Generator

- this repository is dependent on either the "pyro" or the "cinco-language-server"-repository
- it is not intended to be standalone, but rather a submodule
- if one wants to contribute, it is recommended to checkout "cinco-language-server" and change the submodule
of the "pyro-generator" to the desired feature-branch
- please visit  [cinco-cloud](https://scce.gitlab.io/cinco-cloud/content/developer-guide/overview/) for more context.