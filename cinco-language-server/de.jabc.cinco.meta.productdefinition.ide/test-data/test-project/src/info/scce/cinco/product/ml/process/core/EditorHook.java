package info.scce.cinco.product.ml.process.core;

public class EditorHook extends info.scce.pyro.api.PyroEditorHook {
	public void execute(entity.core.PyroEditorGridDB grid) {
		return;
		/*
			// explorer
			final entity.core.PyroEditorGridItemDB explorerItem = getEditorLayoutService()
					.createGridArea(grid, 0L, 0L, 3L, 3L);
			final entity.core.PyroEditorWidgetDB explorerWidget = getEditorLayoutService()
					.createWidget(grid, explorerItem, "Explorer", "explorer");
			explorerItem.widgets.add(explorerWidget);
	
			// canvas
			final entity.core.PyroEditorGridItemDB canvasItem = getEditorLayoutService()
					.createGridArea(grid, 3L, 0L, 6L, 6L);
			final entity.core.PyroEditorWidgetDB canvasWidget = getEditorLayoutService()
					.createWidget(grid, canvasItem, "Canvas", "canvas");
			canvasItem.widgets.add(canvasWidget);
	
			// properties
			final entity.core.PyroEditorGridItemDB propertiesItem = getEditorLayoutService()
					.createGridArea(grid, 3L, 6L, 6L, 3L);
			final entity.core.PyroEditorWidgetDB propertiesWidget = getEditorLayoutService()
					.createWidget(grid, propertiesItem, "Properties", "properties");
			propertiesItem.widgets.add(propertiesWidget);
	
			// palette
			final entity.core.PyroEditorGridItemDB paletteItem = getEditorLayoutService()
					.createGridArea(grid, 9L, 0L, 3L, 3L);
			final entity.core.PyroEditorWidgetDB paletteWidget = getEditorLayoutService()
					.createWidget(grid, paletteItem, "Palette", "palette");
			paletteItem.widgets.add(paletteWidget);
	
			// checks
			final entity.core.PyroEditorGridItemDB checksItem = getEditorLayoutService()
					.createGridArea(grid, 9L, 3L, 3L, 3L);
			final entity.core.PyroEditorWidgetDB checksWidget = getEditorLayoutService()
					.createWidget(grid, checksItem, "Checks", "checks");
			checksItem.widgets.add(checksWidget);
	
			// Ecore
			final entity.core.PyroEditorGridItemDB ecoreItem = getEditorLayoutService()
					.createGridArea(grid, 0L, 3L, 3L, 3L);
			final entity.core.PyroEditorWidgetDB ecoreWidget = getEditorLayoutService()
					.createWidget(grid, ecoreItem, "Ecore", "plugin_ecore");
			ecoreItem.widgets.add(ecoreWidget);
	
			// put rest to available
			
			//Map
			final entity.core.PyroEditorWidgetDB pluginMapWidget = getEditorLayoutService().createWidget(grid, null, "Map", "map");
			grid.availableWidgets.add(pluginMapWidget);
	
			//cmd history
			final entity.core.PyroEditorWidgetDB cmdHistoryWidget = getEditorLayoutService().createWidget(grid, null, "Command History", "command_history");
			grid.availableWidgets.add(cmdHistoryWidget);
			
			//shared
			final entity.core.PyroEditorWidgetDB pluginSharedWidget = getEditorLayoutService().createWidget(grid, null, "Shared", "plugin_shared");
			grid.availableWidgets.add(pluginSharedWidget);
		*/
	}
}
