package info.scce.cinco.product.base.process;

import info.scce.cinco.product.base.process.baseprocess.BaseProcess;

public abstract class ProcessGenerator {
	public abstract CharSequence generate(BaseProcess bp);
}
