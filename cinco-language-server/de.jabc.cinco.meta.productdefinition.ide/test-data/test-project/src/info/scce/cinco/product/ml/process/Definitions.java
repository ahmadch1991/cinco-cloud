package info.scce.cinco.product.ml.process;

public class Definitions {
	public final static int Y_OFF = 25;
	public final static int X_OFF = 5;
	public final static int X_OFF_SERVICE = 25;
	public final static int PORT_HEIGHT = 20;
	public final static int PORT_WIDTH = 110;
}