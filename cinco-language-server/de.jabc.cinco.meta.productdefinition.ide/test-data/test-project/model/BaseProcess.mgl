id info.scce.cinco.product.base.process
stylePath "model/BaseProcess.style"

graphModel BaseProcess {
	diagramExtension "baseprocess"

	containableElements (
		ControlFlowElement,
		DataSource
	)
	attr EString as name
}

edge ControlFlow {
	style controlFlow
}

@disable(delete,reconnect)
edge BranchConnector {
	style branchConnector
}

abstract container ControlFlowElement {}

abstract container ControlFlowTarget extends ControlFlowElement {
	containableElements (InputPort[0,*])
	incomingEdges (ControlFlow[0,*])
}

abstract container ControlFlowSource extends ControlFlowElement {
	containableElements (OutputPort[0,*])
	outgoingEdges (ControlFlow[0,1])
}

/*
*
* SIBS
*
*/

container StartSIB extends ControlFlowSource {
	style startSIB
}

container EndSIB extends ControlFlowTarget {
	style endSIB("${name}")
	attr EString as name
}

abstract container AbstractBranch extends ControlFlowSource {
	attr EString as name
	incomingEdges (BranchConnector[1,1])
}

container Branch extends AbstractBranch {
	style branch("${name}")
}

abstract container SIB extends ControlFlowTarget {
	outgoingEdges (BranchConnector[0,*])
}

container PutToContextSIB extends SIB {
	style genericSIB("${'PutToContext'}")
}

container FunctionSIB extends SIB {
	style genericSIB("${functionName}")
	attr EString as functionName
	attr EString as importStatement
}

container ProcessSIB extends SIB {
	style genericSIB("${proMod.name}")
	prime this::BaseProcess as proMod
}

abstract container Port {
	attr EString as name
	attr EString as typeName
}

@postCreate("info.scce.cinco.product.base.process.hook.PortPostCreate")
container InputPort extends Port {
	style inputPort("${name}:${typeName}")
	incomingEdges ({DataFlow}[0,1])
}

@postCreate("info.scce.cinco.product.base.process.hook.PortPostCreate")
container OutputPort extends Port {
	style outputPort("${name}:${typeName}")
	outgoingEdges ({DataFlow}[0,*])
}

abstract node DataSource {
	attr EString as typeName
	attr EString as name
	outgoingEdges (DataFlow[0,*])
}

node Variable extends DataSource {
	style variable("${name}:${typeName}")
	incomingEdges (DataFlow[0,*])	
}

container Constant extends DataSource {
	style constant("${value}")
	attr EString as value
}

edge DataFlow {
	style dataFlow
}