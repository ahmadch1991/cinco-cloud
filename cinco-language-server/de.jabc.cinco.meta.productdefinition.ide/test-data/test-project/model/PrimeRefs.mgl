import "model/ExtendedFlowGraph.mgl" as flow
import "model/Hierarchy.mgl" as hierarchy

id info.scce.cinco.product.primerefs
stylePath "model/Attributes.style"

graphModel PrimeRefs {
	diagramExtension "primeRefs"
	
	containableElements(
		// standard containments
		SourceNode, SourceContainer,
		// this without jumpToPrime
	 	PrimeToNode, PrimeToEdge, PrimeToContainer, PrimeToGraphModel,
	 	// this with jumpToPrime
	 	PrimeCToNode, PrimeCToEdge, PrimeCToContainer, PrimeCToGraphModel,
	 	// hierarchy.mgl
	 	PrimeToNodeHierarchy, PrimeToAbstractNodeHierarchy,
		// extendedFlowGraph.mgl without jumpToPrime
	 	PrimeToGraphModelFlow, PrimeToNodeFlow, PrimeToEdgeFlow, PrimeToContainerFlow, PrimeToGraphModelFlow,
		// extendedFlowGraph.mgl with jumpToPrime
	 	PrimeCToNodeFlow, PrimeCToEdgeFlow, PrimeCToContainerFlow, PrimeCToGraphModelFlow
	 )
}

node SourceNode {
	style redCircle

	outgoingEdges (SourceEdge[1,*])
	incomingEdges (SourceEdge[1,*])
}

edge SourceEdge {
	style simpleArrow	
}

container SourceContainer {
	style swimlane("sourceContainer")

	containableElements(*[0,*])
}

/*
 * Prime refs to sources in same graphmodel
 */
node PrimeToNode {
	style greenTextRectangle("PrimeToNode(SourceNode)")
	prime this::SourceNode as pr

	outgoingEdges (SourceEdge[1,*])
	incomingEdges (SourceEdge[1,*])
}

node PrimeToEdge {
	style greenTextRectangle("PrimeToEdge(SourceEdge)")
	prime this::SourceEdge as pr

	outgoingEdges (SourceEdge[1,*])
	incomingEdges (SourceEdge[1,*])
}

node PrimeToContainer {
	style greenTextRectangle("PrimeToContainer(SourceContainer)")
	prime this::SourceContainer as pr
	
	outgoingEdges (SourceEdge[1,*])
	incomingEdges (SourceEdge[1,*])
}

node PrimeToGraphModel {
	style greenTextRectangle("PrimeToGraphModel(PrimeRefs)")
	prime this::PrimeRefs as pr
	
	outgoingEdges (SourceEdge[1,*])
	incomingEdges (SourceEdge[1,*])
}

@jumpToPrime
container PrimeCToNode {
	style greenTextRectangle("PrimeCToNode(SourceNode)")
	prime this::SourceNode as pr

	containableElements(*[0,*])
}

@jumpToPrime
container PrimeCToEdge {
	style greenTextRectangle("PrimeCToEdge(SourceEdge)")
	prime this::SourceEdge as pr
	
	containableElements(*[0,*])
}

@jumpToPrime
container PrimeCToContainer {
	style greenTextRectangle("PrimeCToContainer(SourceContainer)")
	prime this::SourceContainer as pr
	
	containableElements(*[0,*])
}

@jumpToPrime
container PrimeCToGraphModel {
	style greenTextRectangle("PrimeCToGraphModel(PrimeRefs)")
	prime this::PrimeRefs as pr
	
	containableElements(*[0,*])
}

/*
 * Prime refs to sources in other graphmodel
 */

@jumpToPrime
node PrimeToNodeHierarchy {
	style blueTextRectangle("PrimeToNodeHierarchy(D)")
	prime hierarchy::D as pr // NOTE: Node with an inheritance-chain containing abstract nodes 
}

@jumpToPrime
node PrimeToAbstractNodeHierarchy {
	style blueTextRectangle("PrimeToAbstractNodeHierarchy(C)")
	prime hierarchy::C as pr // NOTE: Abstract node with a hierarchy chain
}

node PrimeToNodeFlow {
	style blueTextRectangle("PrimeToNodeFlow(Activity)")
	prime flow::Activity as pr
}

node PrimeToEdgeFlow {
	style blueTextRectangle("PrimeToEdgeFlow(LabeledTransition)")
	prime flow::LabeledTransition as pr
}

node PrimeToContainerFlow {
	style blueTextRectangle("PrimeToContainerFlow(Swimlane)")
	prime flow::Swimlane as pr
}

node PrimeToGraphModelFlow {
	style blueTextRectangle("PrimeToGraphModelFlow(FlowGraph)")
	prime flow::ExtendedFlowGraph as pr
}

@jumpToPrime
container PrimeCToNodeFlow {
	style blueTextRectangle("PrimeCToNodeFlow(Activity)")
	prime flow::Activity as pr
}

@jumpToPrime
container PrimeCToEdgeFlow {
	style blueTextRectangle("PrimeCToEdgeFlow(LabeledTransition)")
	prime flow::LabeledTransition as pr
}

@jumpToPrime
container PrimeCToContainerFlow {
	style blueTextRectangle("PrimeCToContainerFlow(Swimlane)")
	prime flow::Swimlane as pr
}

@jumpToPrime
container PrimeCToGraphModelFlow {
	style blueTextRectangle("PrimeCToGraphModelFlow(FlowGraph)")
	prime flow::ExtendedFlowGraph as pr
}