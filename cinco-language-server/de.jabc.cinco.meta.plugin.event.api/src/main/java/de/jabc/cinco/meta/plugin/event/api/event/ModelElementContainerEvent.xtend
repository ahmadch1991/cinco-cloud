package de.jabc.cinco.meta.plugin.event.api.event

import graphmodel.ModelElementContainer

interface ModelElementContainerEvent<Element extends ModelElementContainer> extends IdentifiableElementEvent<Element> {
	
	// Intentionally left blank
	
}
