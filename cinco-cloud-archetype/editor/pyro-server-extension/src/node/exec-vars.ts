/* eslint-disable header/header */
import * as path from 'path';

import { DATABASE_PASSWORD, DATABASE_URL, DATABASE_USER, INTERNAL_PYRO_PORT } from './environment-vars';

export const serverPath = path.resolve(__dirname, '..', '..', 'pyro-server');
export const serverFile = 'app.jar';
export const cmdExec = 'java';
export const cmdArgs = [
    '-Dquarkus.datasource.jdbc.url="jdbc:postgresql://' + DATABASE_URL + '"',
    '-Dquarkus.datasource.username="' + DATABASE_USER + '"',
    '-Dquarkus.datasource.password="' + DATABASE_PASSWORD + '"',
    '-Dquarkus.http.port=' + INTERNAL_PYRO_PORT,
    '-jar'
];
export const cmdDebugArgs = [
    '-Ddebug'
].concat(cmdArgs);
