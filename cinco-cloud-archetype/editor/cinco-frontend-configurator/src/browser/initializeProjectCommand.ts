/* eslint-disable header/header */
export const initializeProjectCommand = {
    id: 'info.scce.cinco-cloud.initialize-project.call',
    label: 'Initialize Project…',
    triggers: 'info.scce.cinco-cloud.initialize-project'
};

export const openFilePickerCommand = {
    id: 'info.scce.cinco-cloud.open-file-picker',
    label: 'Open a PickerDialog...'
};
