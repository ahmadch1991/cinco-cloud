import * as vscode from 'vscode';
import { disposeAll } from './dispose';
import { PyroDocument } from './pyroDocument';
import { EXTERNAL_PYRO_HOST, EXTERNAL_PYRO_PORT, EXTERNAL_PYRO_SUBPATH, EXTERNAL_USE_SSL } from './env_var';
import { PyroApi } from './pyroApi';
import { getExtension, getExtensionFrom, getFileNameFrom, isEmpty } from './fileNameUtils';
import { GraphModelFile } from './graphmodelFile';
import fs = require('fs');

const LOG_NAME = "PYRO-CLIENT";
export const outputChannel = vscode.window.createOutputChannel(LOG_NAME);

export class PyroEditorProvider extends PyroApi implements vscode.CustomEditorProvider<PyroDocument> {

	private static readonly viewType = 'scce.pyro';
	private static webviewOptions: vscode.WebviewOptions =  {
		enableScripts: true,
	};
	private readonly _onDidChangeCustomDocument = new vscode.EventEmitter<vscode.CustomDocumentEditEvent<PyroDocument>>();
	public readonly onDidChangeCustomDocument = this._onDidChangeCustomDocument.event;
	static TIMEOUT_COUNTER = 20;
	static TIMEOUT_TIME = 3000;

	constructor(
		private readonly _context: vscode.ExtensionContext
	) {
		super();
		if(!PyroEditorProvider.isSupportedEditor()) {
			return;
		}
		this.PROJECT_ID = 1;
		vscode.window.onDidChangeActiveTextEditor( (editor: vscode.TextEditor | undefined) => {
				PyroEditorProvider.switchToPyroEditor(editor);
			} 	
		);
		vscode.workspace.onWillDeleteFiles( (e: any) => e.waitUntil(this.removeModelReference(e)));
		_context.subscriptions.push(
			vscode.commands.registerCommand(
				"scce.pyro.createModel",
				(e: any) => this.contextCreateModelTypes(e)
			)
		);
		PyroEditorProvider.registerConnection();
	}

	public static register(context: vscode.ExtensionContext): vscode.Disposable {
		return vscode.window.registerCustomEditorProvider(
			PyroEditorProvider.viewType,
			new PyroEditorProvider(context),
			{
				// For this demo extension, we enable `retainContextWhenHidden` which keeps the 
				// webview alive even when it is not visible. You should avoid using this setting
				// unless is absolutely required as it does have memory overhead.
				webviewOptions: {
					//retainContextWhenHidden: true,
				},
				supportsMultipleEditorsPerDocument: false,
		});
	}

	private static isUnsupportedEditor(): boolean {
		const env = process.env;
		const editorType = env["EDITOR_TYPE"];
		if(editorType != null && editorType == "LANGUAGE_EDITOR") {
			this.logging("EditorType is unsuported: " + editorType);
			return true;
		}
		return false;
	}

	private static isSupportedEditor(): boolean {
		// if the current instance holds a language editor, don't try to activate the functionality
		if(this.isUnsupportedEditor()) {
			vscode.commands.executeCommand('setContext', "isSupportedEditor", false);
			vscode.commands.executeCommand('setContext', "isConnectedToPyro", false);
			return false;
		}
		return true;
	}

	public static registerConnection() {
		vscode.commands.executeCommand('setContext', "isSupportedEditor", true);
		// try to activate the functionality to create a model
		PyroEditorProvider.checkRegisterConnection(
			PyroEditorProvider.TIMEOUT_COUNTER,
			PyroEditorProvider.TIMEOUT_TIME
		);
	}

	private static checkRegisterConnection(timeoutCounter: number, timeoutTime: number) {
		PyroApi.isRunning().then(() => {
			vscode.commands.executeCommand('setContext', "isConnectedToPyro", true);
		}).catch(() => {
			this.logging("Could not connect to Pyro-Server. [" + timeoutCounter + "] Retrying after " + timeoutTime + "ms...");
			setTimeout(() => {
				if(timeoutCounter <= 0) {
					vscode.commands.executeCommand('setContext', "isConnectedToPyro", false);
					this.logging("The connection to the Pyro server could not be established.");
				} else {
					this.checkRegisterConnection(timeoutCounter - 1, timeoutTime);
				}
			}, timeoutTime);
		});
	}

	static switchToPyroEditor(editor: vscode.TextEditor | undefined) {
		if(editor && editor.document && !editor.document.isClosed) {
			const document = editor.document;
			this.openDocumentInPyro(document);
		}
	}

	static async openDocumentInPyro(document: vscode.TextDocument) {
		const fileName = document.fileName;
		const fileComponents = fileName.split(".");
		if(fileComponents.length > 0) {
			const fileType = fileComponents[fileComponents.length - 1];
			const modelTypes = await this.getModelTypes();
			const fileTypes = Object.entries(modelTypes);

			for(const t of fileTypes) {
				// if fileType is a registered graphModel-fileType
				if(t[1] == fileType) {
					this.logging("Switching to Pyro-Editor: "+document.uri.fsPath);
					await vscode.commands.executeCommand('workbench.action.closeActiveEditor');
					await vscode.commands.executeCommand('vscode.openWith', document.uri, this.viewType);
					break;
				}
			}
		}
	}

	async isPyroCompatible(filePath: string) {
		const fileExtension = getExtensionFrom(filePath);
		if(fileExtension.length > 0) {
			const modelTypes = await PyroEditorProvider.getModelTypes();
			const fileTypes = Object.entries(modelTypes);
			for(const t of fileTypes) {
				if(t[1] == fileExtension) {
					return true;
				}
			}
		}
		return false;
	}

	async openCustomDocument(
		uri: vscode.Uri,
		openContext: { backupId?: string },
		_token: vscode.CancellationToken
	): Promise<PyroDocument> {
		this.logging("Trying to open as PYRO-DOCUMENT: "+uri.fsPath);

		const document: PyroDocument = await PyroDocument.create(uri, openContext.backupId);
		const listeners: vscode.Disposable[] = [];

		listeners.push(document.onDidChange( (e: any) => {
			// Tell VS Code that the document has been edited by the use.
			this._onDidChangeCustomDocument.fire({
				document,
				...e
			});
		}));
		document.onDidDispose(() => disposeAll(listeners));
		return document;
	}

	async resolveCustomEditor(
		document: PyroDocument,
		webviewPanel: vscode.WebviewPanel,
		_token: vscode.CancellationToken
	): Promise<void> {
		this.logging("Resolving PYRO-EDITOR: "+document.delegate.uri.fsPath);
		// only pyro compatible iff fileType of document is a registered one

		if(!await this.isPyroCompatible(document.delegate.fileName)) {
			this.logging("Not compatible to Pyro: "+document.delegate.uri.fsPath);
			await vscode.commands.executeCommand('workbench.action.closeActiveEditor');
			await vscode.commands.executeCommand('vscode.openWith', document.delegate.uri, 'default');
			this.logging("openedWith passed: "+document.delegate.uri.fsPath);
			return;
		}
		this.logging("Compatible to Pyro: "+document.delegate.uri.fsPath);

		// read query-information
		const jsonDocument = await this.getDocumentAsJson(document);

		// Setup initial content for the webview
		const editorWebview = webviewPanel.webview;
		editorWebview.options = PyroEditorProvider.webviewOptions;
		const token = await PyroApi.getJWT();
		editorWebview.html = this.getHtmlForWebview(jsonDocument, token);
		editorWebview.onDidReceiveMessage(e => this.onWebviewEditorMessage(document, e));
	}

	private async getDocumentAsJson(document: PyroDocument) : Promise<any> {
		const text = document.documentData;
		if (isEmpty(text)) {
			return this.createModelReference(document);
		} else {
			try {
				return JSON.parse(text);
			} catch {
				throw new Error("Could not parse modelReference!s");
			}
		}
	}

	private async createModelReference(document: PyroDocument) {
		const filename = getFileNameFrom(document.delegate.fileName);
		const extension = getExtension(filename);

		// new file, store model id
		const modelTypes = await PyroEditorProvider.getModelTypesOf(extension);
		const modelType = modelTypes[0];
		const model = await PyroEditorProvider.createModel(filename, modelType);
		const modelReference: GraphModelFile = {
			id: model.id,
			modelType: modelType,
			fileExtension: extension
		};
		this.updateTextDocument(document, modelReference);
		return modelReference;
	}

	private removeModelReference(e: vscode.FileWillDeleteEvent): Thenable<vscode.WorkspaceEdit> {
		const files = e.files;
		return new Promise((resolve, reject) => {
			for(const file of files) {
				this.isPyroCompatible(file.fsPath).then( (compatible) => {
					if(compatible) {
						vscode.workspace.openTextDocument(file).then((document) => {
							const content = document.getText();
							const model: GraphModelFile = JSON.parse(content);
							PyroEditorProvider.removeModel(model.modelType, model.id).then( () => {
								this.logging("Successfully removed graphModel:\n"+model);
							}).catch((e) => {
								this.logging("Failed to removed graphModel:\n"+model);
								this.logging("ERROR: "+e);
							});
							resolve(true);
						});
					} else {
						resolve(false);
					}
				});
			}
		}).then();
	}

	public async getSelected(e: any) {
		let workspacePath = process.env.WORKSPACE_PATH ? process.env.WORKSPACE_PATH : "/editor/workspace";
		let selectedPath = (e!=null ? e.fsPath : "") as string ;
		workspacePath = this.replaceAll(workspacePath, "\\", "/");
		selectedPath = this.replaceAll(selectedPath, "\\", "/");
		if(!fs.existsSync( selectedPath )) {
			this.logging("Selected path unsupported: " + selectedPath);
			this.logging("Falling back to: " + workspacePath);
			selectedPath = workspacePath;
		} else if(!fs.lstatSync( selectedPath ).isDirectory()) {
			this.logging("Selected path is a file: " + selectedPath + "\nFalling back to folder.");
			const lastComponentIndex = selectedPath.lastIndexOf("/");
			selectedPath = selectedPath.substring(0, lastComponentIndex + 1);
		}
		this.logging("Selected path: "+selectedPath);
		return selectedPath;
	}

	//Taken from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
	public escapeRegExp(str: string): string {
		return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
	}

	public replaceAll(str: string, match: string, replacement: string): string {
		return str.replace(new RegExp(this.escapeRegExp(match), 'g'), ()=>replacement);
	}

	public async contextCreateModelTypes(e: any) {
		const path = await this.getSelected(e);
		if(!path) {
			vscode.window.showErrorMessage("No path for file specified. Please right click on a folder or workspace to create a Graphmodel!");
			return;
		}
		PyroApi.getModelTypes().then((types: Map<string, string>) => {
			const items: string[] = [];
			for (const entry of Object.entries(types)) {
				items.push(entry[1] + " ("+entry[0]+")");
			}
			vscode.window.showQuickPick(items, {
				title: "Model Type",
				placeHolder: "please pick a type...",
				canPickMany: false,
			}).then((value: any) => {
				const fileExtension = value?.split(" (")[0];
				vscode.window.showInputBox({
					title: "Model Name",
					placeHolder: "myModel (for myModel."+fileExtension+")",
					prompt: "Please type a name for your model."
				}).then( (name: any) => {
					if(name) {
						this.logging("creating: "+name+"."+fileExtension);
						const fileName = name+"."+fileExtension;
						const newFile = vscode.Uri.file(path+'/'+fileName);
						vscode.workspace.fs.writeFile(newFile, new Uint8Array()).then( (v) => {
							vscode.workspace.openTextDocument(newFile).then(document => {
								const edit = new vscode.WorkspaceEdit();
								edit.insert(newFile, new vscode.Position(0, 0), "");
								return vscode.workspace.applyEdit(edit).then(success => {
									if (success) {
										vscode.window.showTextDocument(document);
									} else {
										vscode.window.showErrorMessage('Error, Could not create file!');
									}
								});
							});
						});
					}
				});
			});
		});
	}

	/**
	 * Write out the json to a given document.
	 */
	private async updateTextDocument(document: PyroDocument, json: any) {
		const uri = document.uri;
		const data = JSON.stringify(json, null, 2);
		fs.writeFile(uri.fsPath, data, (err: any) => {
			if (err) this.logging(err);
			this.logging("Successfully Written to File - "+uri.fsPath);
		});
	}

	/**
	 * Get the static HTML used for in our editor's webviews.
	 */
	private getHtmlForWebview(jsonDocument: any, token: string): string {
		const modelId = jsonDocument.id.toString();
		const fileExtension = jsonDocument.fileExtension.toString();
		const protocol = EXTERNAL_USE_SSL ? 'https' : 'http';
        const pyroUrl =
            `${protocol}://${EXTERNAL_PYRO_HOST}${EXTERNAL_PYRO_PORT ? `:${EXTERNAL_PYRO_PORT}` : ''}` +
            `${EXTERNAL_PYRO_SUBPATH}/#/editor/${modelId}` +
            `?ext=${fileExtension}&token=${token}`;
		this.logging(`accessing: ${pyroUrl}`);
		return `
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8"/>
				<title>Pyro Model Editor</title>
				<meta
                    http-equiv="Content-Security-Policy"
                    content="
                        default-src *;
                        font-src data:;
                        img-src *;
                        script-src *;
                        script-src-elem * 'nonce-2726c7f26c';
                        style-src * 'unsafe-inline';
                    "
                >
			</head>
			<body style="padding: 0px;">
				<iframe
                    id="pyro_editor"
                    src="${pyroUrl}"
                    title="Pyro editor iframe"
                    style="position:absolute; width:100%; height:100%; border:none; margin:0; padding:0;"
                >
                    Not Supported
                </iframe>
				<script nonce="2726c7f26c">

					const vscode = acquireVsCodeApi();

					window.addEventListener('message', event => {
						const message = event.data; // The json data that the extension sent
						document.getElementById('pyro_editor').contentWindow.postMessage(message);
					});
					document.getElementById('pyro_editor').contentDocument.onmessage = function(m) {
						vscode.postMessage(m);
					}
				</script>
			</body>
		</html>
		`;	
	}

	/**
	 * CURRENTLY NOT NEEDED
	 */

	private onWebviewEditorMessage(document: PyroDocument, e: any) {
		this.logging(e);
		if(e.type=="changed") {
			document.makeEdit();
		}
	}

	backupCustomDocument(document: PyroDocument, context: vscode.CustomDocumentBackupContext, cancellation: vscode.CancellationToken): Thenable<vscode.CustomDocumentBackup> {
		return document.backup(context.destination, cancellation);
	}
	
	public saveCustomDocument(document: PyroDocument, cancellation: vscode.CancellationToken): Thenable<void> {
		return document.save(cancellation);
	}
	
	public saveCustomDocumentAs(document: PyroDocument, destination: vscode.Uri, cancellation: vscode.CancellationToken): Thenable<void> {
		return document.saveAs(destination, cancellation);
	}

	public revertCustomDocument(document: PyroDocument, cancellation: vscode.CancellationToken): Thenable<void> {
		return document.revert(cancellation);
	}

	public static logging(input: any) {
		const LOG = "["+LOG_NAME+"]";
		console.log(LOG+":"+input);
		outputChannel.appendLine(input);
	}

	public logging(input: any) {
		PyroEditorProvider.logging(input);
	}
}
